unit dmData;

interface

uses
  System.SysUtils, System.Classes, Data.DB, adsdata, adsfunc, adstable, adsset, adscnnct;

type
  TDataModule1 = class(TDataModule)
    tblTagGroup: TAdsTable;
    POSConnection: TAdsConnection;
    AdsSettings1: TAdsSettings;
    procedure POSConnectionBeforeConnect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModule1: TDataModule1;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDataModule1.POSConnectionBeforeConnect(Sender: TObject);
begin
  POSConnection.ConnectPath := ExtractFilePath(ParamStr(0));
end;

end.

unit uformGroupSetup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, cxTextEdit, Vcl.StdCtrls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxTokenEdit, Vcl.Samples.Spin, cxMaskEdit, cxDropDownEdit, cxColorComboBox, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, Vcl.ExtCtrls, Utils, math, dximctrl,
  cxImageComboBox, LMDControl, LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit,
  LMDCustomBrowseEdit, LMDCustomFileEdit, LMDFileOpenEdit, Vcl.DBCtrls, Vcl.Buttons, dxDateRanges, Vcl.ComCtrls, dxtree,
  dxdbtree, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTLData, cxDBTL, Vcl.AppEvnts;

type
  TmyColrList = record
    Name: string;
    hex500: string;
    hexA100: string;
  end;



  TfrmGroupSetup = class(TForm)
    dsTagGroup: TDataSource;
    TableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    pnlCard: TPanel;
    btnCardFont: TLMDSpeedButton;
    btnCardSample: TLMDSpeedButton;
    lblCard: TLabel;
    chkCardCode: TCheckBox;
    chkCardPrice: TCheckBox;
    cxColorComboBoxCard: TcxColorComboBox;
    pnlButton: TPanel;
    btnCardfont01: TLMDSpeedButton;
    btnSample: TLMDSpeedButton;
    lblButton: TLabel;
    btnUpdate: TButton;
    pnlMisc: TPanel;
    lblMiscHeading: TLabel;
    lblinfo2: TLabel;
    chkshowInactive: TCheckBox;
    lbledtCardPreCode: TLabeledEdit;
    chkUseModifier: TCheckBox;
    pnlSortby: TPanel;
    lblSortby: TLabel;
    icbSort: TcxImageComboBox;
    lbledtPicture: TLMDLabeledFileOpenEdit;
    DBNavigator1: TDBNavigator;
    pnlTagfilter: TPanel;
    SourceTag: TLabel;
    edtTagList: TEdit;
    pnlBotm: TPanel;
    edtTestfilter: TEdit;
    lblfilter: TLabel;
    btnRefresh: TSpeedButton;
    dxDBTreeView1: TdxDBTreeView;
    btnSibling: TButton;
    btnParent: TButton;
    pnlDescrip: TPanel;
    lblShowsup: TLabel;
    pnlType: TPanel;
    lblType: TLabel;
    icbTable: TcxImageComboBox;
    lbledtCaption: TEdit;
    lblCaption: TLabel;
    lblCardDescLines: TLabel;
    seDescLines: TSpinEdit;
    seCardWidth: TSpinEdit;
    lblCardWidth: TLabel;
    ApplicationEvents1: TApplicationEvents;
    lblParenValue: TLabel;
    lbMyTag: TLabel;
    lblMyTagValue: TLabel;
    pnlMyTag: TPanel;
    lbledtTagFilter: TEdit;
    lblSelfTag: TLabel;

    procedure cxColorComboBoxCardPropertiesChange(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure dsTagGroupDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure seCardWidthChange(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure dxDBTreeView1Click(Sender: TObject);
    procedure btnSiblingClick(Sender: TObject);
    procedure btnParentClick(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
  private
    procedure colrfil;
    procedure FilterByTag(sFilter,Sfield: string);
    function GuidIdString: string;
    { Private declarations }
  public

    { Public declarations }


  end;

var
  frmGroupSetup: TfrmGroupSetup;
  FGuidLastTimePart: Integer ;


const
  SWindowClassName = 'XBrowse2RegisterHelperApp';
  clrList: array [1 .. 21] of TmyColrList = ((name: 'Red'; hex500: '$F44336'; hexA100: '$FF8A80'), (name: 'Pink';
    hex500: '$E91E63'; hexA100: '$FF80AB'), (name: 'Purple'; hex500: '$9C27B0'; hexA100: '$EA80FC'),
    (name: 'Deep Purple'; hex500: '$673AB7'; hexA100: '$B388FF'), (name: 'Indigo'; hex500: '$3F51B5';
    hexA100: '$8C9EFF'), (name: 'Blue'; hex500: '$2196F3'; hexA100: '$82B1FF'), (name: 'Light Blue'; hex500: '$03A9F4';
    hexA100: '$80D8FF'), (name: 'Cyan'; hex500: '$00BCD4'; hexA100: '$84FFFF'), (name: 'Teal'; hex500: '$009688';
    hexA100: '$A7FFEB'), (name: 'Green'; hex500: '$4CAF50'; hexA100: '$B9F6CA'), (name: 'Light Green';
    hex500: '$8BC34A'; hexA100: '$CCFF90'), (name: 'Lime'; hex500: '$CDDC39'; hexA100: '$F4FF81'), (name: 'Yellow';
    hex500: '$FFEB3B'; hexA100: '$FFFF8D'), (name: 'Amber'; hex500: '$FFC107'; hexA100: '$FFE57F'), (name: 'Orange';
    hex500: '$FF9800'; hexA100: '$FFD180'), (name: 'Deep Orange'; hex500: '$FF5722'; hexA100: '$FF9E80'),
    (name: 'Brown'; hex500: '$795548'; hexA100: '$A1887F'), (name: 'Grey'; hex500: '$9E9E9E'; hexA100: '$BDBDBD'),
    (name: 'Blue Grey'; hex500: '$607D8B'; hexA100: '$CFD8DC'), (name: 'Black'; hex500: '$000000'; hexA100: '$000000'),
    (name: 'White'; hex500: '$FFFFFF'; hexA100: '$FFFFFF'));

implementation

//uses PrdData, MainBrowse;


uses dmData;
{$R *.dfm}

procedure TfrmGroupSetup.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
var s: string;
begin
 // btnTagsetup.Enabled := (icbTable.ItemIndex=1) and (lbledtTagFilter.Text >'')  ;
  s:= IIF(icbTable.ItemIndex=1,'My Tag - Parent of Sub-Menu', 'My Tag - Use same TAG as Items to display') ;
  lblSelfTag.Caption := s ;
  chkCardCode.Visible :=   (icbTable.ItemIndex=0) ;
  chkCardPrice.Visible :=  (icbTable.ItemIndex=0) ;
  //chkCardAsGrid.Visible := (icbTable.ItemIndex=0) ;
  //pnlGrid.Visible :=        (icbTable.ItemIndex=0) ;
  //pnlCard.Visible := not    chkCardAsGrid.Checked ;

end;

procedure TfrmGroupSetup.btnParentClick(Sender: TObject);
var  AColumn: TcxGridColumn;
begin
     //    AColumn := TableView1.GetColumnByFi eldName('Card);
   //edtTestfilter.Text :=  lbledtTagFilter.Text ;
  FilterByTag(edtTagList.Text,'My_Tag');
  {
      if Assigned(AColumn) then
      AColumn.Visible := not  (edtTestfilter.Text >'') ;
       pnlTagfilter.Visible := not  (edtTestfilter.Text >'') ;
       }
  TableView1.ApplyBestFit;
end;

procedure TfrmGroupSetup.btnRefreshClick(Sender: TObject);
var  AColumn: TcxGridColumn;
begin

  AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
  FilterByTag(edtTestfilter.Text,'PARENT_TAG');
      if Assigned(AColumn) then
      AColumn.Visible := not  (edtTestfilter.Text >'') ;
       pnlTagfilter.Visible := not  (edtTestfilter.Text >'') ;
  TableView1.ApplyBestFit;
end;

procedure TfrmGroupSetup.btnSiblingClick(Sender: TObject);
begin
         edtTestfilter.Text :=  edtTagList.Text  ;
         btnRefreshClick(nil);
end;

procedure TfrmGroupSetup.btnUpdateClick(Sender: TObject);
var
  ADataSet: TDataSet;
  sCaption, sTagSort, sTable, sPicture, sTagFilter, sCardPreCode, sTagList: string;
  bCardCode, bCardPrice, bshowInactive, bUseModifier: Boolean;
  iDescLines, iCardWidth, iCardColor: Integer;
begin
  if lbledtTagFilter.Text ='' then
     begin
       ShowMessage('My-Tag Cant be empty' );
       Exit
     end;


  ADataSet := dsTagGroup.DataSet;
  if edtTestfilter.Text>'' then
     edtTagList.Text := edtTestfilter.Text;


  sCaption := lbledtCaption.Text;
  sTagSort := VarToStr(icbSort.EditValue);

  sTable := VarToStr(icbTable.EditValue);
  sPicture := lbledtPicture.Text;
  sTagFilter := lbledtTagFilter.Text;
  sCardPreCode := lbledtCardPreCode.Text;
  sTagList := edtTagList.Text ;
  bCardCode := chkCardCode.Checked;
  bCardPrice := chkCardPrice.Checked;
  bshowInactive := chkshowInactive.Checked;
  bUseModifier := chkUseModifier.Checked;

  iCardColor := cxColorComboBoxCard.ColorValue;
  iDescLines := seDescLines.Value;
  iCardWidth := seCardWidth.Value;

  ADataSet.Edit;
  ADataSet.FieldByName('Desc').AsString := sCaption;
  ADataSet.FieldByName('CardWidth').AsInteger := iCardWidth;
  ADataSet.FieldByName('CardSortBy').AsString := sTagSort;
  ADataSet.FieldByName('CardDescLines').AsInteger := iDescLines;
  ADataSet.FieldByName('output').AsString := sTable;
  ADataSet.FieldByName('PARENT_TAG').AsString := sTagList;


  ADataSet.FieldByName('CardShowCode').AsBoolean := bCardCode;
  ADataSet.FieldByName('CardShowPrice').AsBoolean := bCardPrice;
  ADataSet.FieldByName('CardColor').AsInteger := iCardColor ;
  ADataSet.FieldByName('CardPicture').AsString := sPicture;
  ADataSet.FieldByName('My_tag').AsString := sTagFilter;
  ADataSet.FieldByName('CardShowInactive').AsBoolean := bshowInactive;
  ADataSet.FieldByName('CardPreString').AsString := sCardPreCode;
  ADataSet.FieldByName('CardUseModifier').AsBoolean := bUseModifier;
  if ADataSet.FieldByName('code').AsString.IsEmpty then
     ADataSet.FieldByName('code').AsString :=  GuidIdString;
  ADataSet.Post;
end;

procedure TfrmGroupSetup.cxColorComboBoxCardPropertiesChange(Sender: TObject);
begin
  // btnSample.Color := cxColorComboBox01.ColorValue;
  btnCardSample.Color := iif(cxColorComboBoxCard.ColorValue > 0, cxColorComboBoxCard.ColorValue, btnCardSample.Color);
end;

procedure TfrmGroupSetup.dsTagGroupDataChange(Sender: TObject; Field: TField);
var
  ADataSet: TDataSet;
  aI: Integer;

begin
  ADataSet := dsTagGroup.DataSet;
  seCardWidth.Value := ADataSet.FieldByName('CardWidth').AsInteger;
  lbledtCaption.Text := ADataSet.FieldByName('Desc').AsString;
  icbSort.EditValue := LowerCase(ADataSet.FieldByName('CardSortBy').AsString);
  seDescLines.Value := ADataSet.FieldByName('CardDescLines').AsInteger;

  icbTable.EditValue := LowerCase(ADataSet.FieldByName('output').AsString);
  if icbTable.ItemIndex=-1 then            // if empty or not in list default to ITEM
    icbTable.ItemIndex := 0;

  chkCardCode.Checked := ADataSet.FieldByName('CardShowCode').AsBoolean;
  chkCardPrice.Checked := ADataSet.FieldByName('CardShowPrice').AsBoolean;
  aI := ADataSet.FieldByName('CardColor').AsInteger;
  cxColorComboBoxCard.ColorValue := iif(aI > 0, aI, cxColorComboBoxCard.ColorValue);
  lbledtPicture.Text := ADataSet.FieldByName('CardPicture').AsString;
  lbledtTagFilter.Text := ADataSet.FieldByName('My_TAG').AsString;
  edtTagList.Text := ADataSet.FieldByName('PARENT_TAG').AsString;
  chkshowInactive.Checked := ADataSet.FieldByName('CardShowInactive').AsBoolean;
  lbledtCardPreCode.Text := ADataSet.FieldByName('CardPreString').AsString;
  chkUseModifier.Checked := ADataSet.FieldByName('CardUseModifier').AsBoolean;

  lblParenValue.Caption := ADataSet.FieldByName('PARENT_TAG').AsString;
  lblMyTagValue.Caption := ADataSet.FieldByName('My_Tag').AsString;

end;

procedure TfrmGroupSetup.dxDBTreeView1Click(Sender: TObject);
var
  ADataSet: TDataSet;
    AColumn: TcxGridColumn;
begin
     ADataSet := dsTagGroup.DataSet;
          AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
          edtTestfilter.Text := ADataSet.FieldByName('PARENT_TAG').AsString;
  FilterByTag(edtTestfilter.Text,'PARENT_TAG');
      if Assigned(AColumn) then
      AColumn.Visible := not  (edtTestfilter.Text >'') ;
       pnlTagfilter.Visible := not  (edtTestfilter.Text >'') ;
  TableView1.ApplyBestFit;
end;


procedure TfrmGroupSetup.FilterByTag(sFilter,sField: string);
begin
  DataModule1.tblTagGroup.DisableControls;
  try
    DataModule1.tblTagGroup.Filtered := False;
    if sFilter='' then
      DataModule1.tblTagGroup.Filter := ''
    else
    begin
     DataModule1.tblTagGroup.Filter := Format('Contains(%s, %s)', [sField ,QuotedStr(sFilter)]);
     //dmPrdData.tblTagGroup.Filter := Format('Contains(TagList, %s)', [QuotedStr(sFilter)]   );
      DataModule1.tblTagGroup.Filtered := True;
    end;
  finally
    DataModule1.tblTagGroup.EnableControls;
  end;
end;

procedure TfrmGroupSetup.FormShow(Sender: TObject);
var
  AColumn: TcxGridColumn;
begin
  colrfil();
  DataModule1.POSConnection.IsConnected := True;
  DataModule1.tblTagGroup.Active := True;
  TableView1.DataController.CreateAllItems;

 {
  AColumn := TableView1.GetColumnByFieldName('TagList');
  if Assigned(AColumn) then
  begin
    AColumn.PropertiesClass := TTagTokenEditorProperties;
    AColumn.Caption := 'Parent Tag' ;
    //TTagTokenEditorProperties(AColumn.Properties).PopupDataSource
  end;


  AColumn := TableView1.GetColumnByFieldName('CardFilterTag');
  if Assigned(AColumn) then
    begin
    AColumn.PropertiesClass := TTagTokenEditorProperties;
    AColumn.Caption := 'My Tag' ;
  end;
  }
  AColumn := TableView1.GetColumnByFieldName('LineID');
  if Assigned(AColumn) then
    AColumn.Visible := False;

  AColumn := TableView1.GetColumnByFieldName('SmlDesc');
  if Assigned(AColumn) then
    AColumn.Visible := False;

  AColumn := TableView1.GetColumnByFieldName('Price');
  if Assigned(AColumn) then
    AColumn.Visible := False;

  AColumn := TableView1.GetColumnByFieldName('code');
  if Assigned(AColumn) then
    AColumn.Visible := False;


  AColumn := TableView1.GetColumnByFieldName('cat');
  if Assigned(AColumn) then
    AColumn.Visible := False;

  if  edtTestfilter.Text >'' then
  begin
       pnlTagfilter.Visible := False ;
     AColumn := TableView1.GetColumnByFieldName('PARENT_TAG');
     FilterByTag(edtTestfilter.Text,'PARENT_TAG');
     if Assigned(AColumn) then
        AColumn.Visible :=  False ;
  end;
  TableView1.ApplyBestFit;
{
 dxDBTreeView1.KeyField := 'code' ;
 dxDBTreeView1.ListField := 'TagList' ;
 dxDBTreeView1.DisplayField := 'Desc' ;
 dxDBTreeView1.ParentField := 'CardFilterTag' ;
 }

dxDBTreeView1.KeyField := 'code' ;
 dxDBTreeView1.ListField := 'My_Tag' ;
 dxDBTreeView1.DisplayField := 'Desc' ;
 dxDBTreeView1.ParentField := 'PARENT_TAG' ;



   // TagTokenEditor1.Visible := False ; //not used yet
end;

procedure TfrmGroupSetup.seCardWidthChange(Sender: TObject);
begin
  btnCardSample.Width := Max(seCardWidth.Value, 75);
end;

procedure TfrmGroupSetup.colrfil();

var
  AColor: TColor;
  intcolor: Integer;
  i: Integer;
begin
  TcxColorComboBoxProperties(cxColorComboBoxCard.Properties).NamingConvention := cxncNone;

  // AColor := RGB(255,255,1);  // Yellow

  for i := 1 to Length(clrList) do
  begin
    intcolor := clrList[i].hexA100.ToInteger();
    AColor := RGB(GetBValue(intcolor), GetGValue(intcolor), GetRValue(intcolor)); // red
    // TcxColorComboBoxProperties(cxColorComboBox01.Properties).CustomColors.AddColor(AColor, clrList[i].Name);
    TcxColorComboBoxProperties(cxColorComboBoxCard.Properties).CustomColors.AddColor(AColor, clrList[i].Name);
  end;


end;


function TfrmGroupSetup.GuidIdString: string;

var
  Year, Month, Day: Word;
  CurTimePart: Integer;
begin

  DecodeDate(Now, Year, Month, Day);
  Result := '';
  //Result := Result + LocationCode; // 1 LocationCode
  //Result := Result + StationCode; // 1 StationCode
  Result := Result + IntToHex(Year - 2000, 1); // 1 Year - in year 2016 it will addtnl digit
  Result := Result + Chr(Month + 64); // 1 Month
  if Day < 10 then
    Result := Result + IntToStr(Day) // 1 Day in Month
  else
    Result := Result + Chr(Day + 55);
  CurTimePart := Round(Frac(Now) * 24 * 60 * 60);
  FGuidLastTimePart := Max(CurTimePart, FGuidLastTimePart + 1);
  Result := Result + IntToHex(FGuidLastTimePart, 5); // 5 Time
  Result := Result + IntToHex(RandomRange(1, 32), 2); // 2 digit Random
end;

 end.

object frmGroupSetup: TfrmGroupSetup
  Left = 0
  Top = 0
  Caption = 'Group Setup'
  ClientHeight = 778
  ClientWidth = 1126
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    AlignWithMargins = True
    Left = 3
    Top = 10
    Width = 1120
    Height = 215
    Margins.Top = 10
    Align = alTop
    TabOrder = 0
    object TableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmAlways
      DataController.DataSource = dsTagGroup
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.Visible = True
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = TableView1
    end
  end
  object pnlCard: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 231
    Width = 398
    Height = 456
    Align = alLeft
    TabOrder = 1
    ExplicitTop = 325
    ExplicitHeight = 362
    object btnCardFont: TLMDSpeedButton
      Left = 29
      Top = 383
      Width = 102
      Height = 23
      Caption = 'Card Font'
      AutoSize = False
      ThemeMode = ttmNone
    end
    object btnCardSample: TLMDSpeedButton
      Left = 210
      Top = 369
      Width = 102
      Height = 51
      Caption = 'Card Sample'
      AutoSize = False
      EnterColor = clBtnFace
      ImageIndex = -1
      ThemeMode = ttmNone
      ButtonStyle = ubsDelphi
    end
    object lblCard: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 13
      Width = 390
      Height = 16
      Margins.Top = 12
      Align = alTop
      Alignment = taCenter
      Caption = 'Card settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ExplicitWidth = 87
    end
    object lblCaption: TLabel
      AlignWithMargins = True
      Left = 25
      Top = 94
      Width = 348
      Height = 13
      Margins.Left = 24
      Margins.Top = 6
      Margins.Right = 24
      Margins.Bottom = 0
      Align = alTop
      Caption = 'Button Caption'
      ExplicitWidth = 72
    end
    object chkCardCode: TCheckBox
      AlignWithMargins = True
      Left = 45
      Top = 290
      Width = 137
      Height = 17
      Margins.Top = 12
      Caption = 'Include Code on card'
      TabOrder = 0
    end
    object chkCardPrice: TCheckBox
      Left = 42
      Top = 258
      Width = 140
      Height = 17
      Caption = 'Include Price on Card'
      TabOrder = 1
    end
    object cxColorComboBoxCard: TcxColorComboBox
      Left = 8
      Top = 324
      BeepOnEnter = False
      Properties.AllowSelectColor = True
      Properties.ClearKey = 46
      Properties.CustomColors = <>
      Properties.DropDownListStyle = lsEditFixedList
      Properties.PrepareList = cxplHTML4
      Properties.OnChange = cxColorComboBoxCardPropertiesChange
      TabOrder = 2
      Width = 190
    end
    object pnlTagfilter: TPanel
      AlignWithMargins = True
      Left = 25
      Top = 140
      Width = 348
      Height = 45
      Margins.Left = 24
      Margins.Top = 6
      Margins.Right = 24
      Align = alTop
      BevelEdges = []
      BevelOuter = bvNone
      TabOrder = 3
      object SourceTag: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 342
        Height = 13
        Align = alTop
        Caption = 'Parent Tag '
        ExplicitWidth = 56
      end
      object edtTagList: TEdit
        AlignWithMargins = True
        Left = 3
        Top = 22
        Width = 342
        Height = 21
        Align = alTop
        Anchors = [akLeft, akBottom]
        TabOrder = 0
      end
    end
    object pnlType: TPanel
      AlignWithMargins = True
      Left = 25
      Top = 44
      Width = 348
      Height = 41
      Margins.Left = 24
      Margins.Top = 12
      Margins.Right = 24
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object lblType: TLabel
        Left = 0
        Top = 0
        Width = 348
        Height = 13
        Margins.Left = 24
        Margins.Top = 12
        Margins.Right = 24
        Align = alTop
        Caption = 'Output Type'
        ExplicitWidth = 61
      end
      object icbTable: TcxImageComboBox
        Left = 0
        Top = 13
        Margins.Left = 24
        Margins.Top = 0
        Margins.Right = 24
        Align = alClient
        EditValue = 'prdmstr'
        Properties.Items = <
          item
            Description = 'ITEMS'
            ImageIndex = 0
            Value = 'prdmstr'
          end
          item
            Description = 'SUB-MENU'
            Value = 'taggroup'
          end>
        TabOrder = 0
        Width = 348
      end
    end
    object lbledtCaption: TEdit
      AlignWithMargins = True
      Left = 25
      Top = 110
      Width = 348
      Height = 21
      Margins.Left = 24
      Margins.Right = 24
      Align = alTop
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Text = 'Caption'
      TextHint = 'Caption'
    end
    object pnlMyTag: TPanel
      AlignWithMargins = True
      Left = 25
      Top = 194
      Width = 348
      Height = 53
      Margins.Left = 24
      Margins.Top = 6
      Margins.Right = 24
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 6
      object lblSelfTag: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 6
        Width = 342
        Height = 13
        Margins.Top = 6
        Align = alTop
        Caption = 'My Tag - Item Tag'
        ExplicitWidth = 88
      end
      object lbledtTagFilter: TEdit
        Left = 0
        Top = 22
        Width = 348
        Height = 21
        Align = alTop
        TabOrder = 0
      end
    end
  end
  object pnlButton: TPanel
    AlignWithMargins = True
    Left = 407
    Top = 231
    Width = 214
    Height = 456
    Align = alLeft
    TabOrder = 2
    ExplicitTop = 325
    ExplicitHeight = 362
    object btnCardfont01: TLMDSpeedButton
      Left = 9
      Top = 304
      Width = 94
      Height = 39
      Caption = 'Button Font'
      AutoSize = False
      ThemeMode = ttmNone
    end
    object btnSample: TLMDSpeedButton
      Left = 9
      Top = 365
      Width = 89
      Height = 44
      ParentCustomHint = False
      Color = clSkyBlue
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
      AutoSize = False
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ThemeMode = ttmNone
      ButtonStyle = ubsDelphi
      MultiLine = True
      StyleElements = []
    end
    object lblButton: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 13
      Width = 206
      Height = 16
      Margins.Top = 12
      Align = alTop
      Alignment = taCenter
      Caption = 'Group settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ExplicitWidth = 95
    end
    object lblCardDescLines: TLabel
      Left = 14
      Top = 161
      Width = 83
      Height = 13
      Caption = 'Description  Lines'
    end
    object lblCardWidth: TLabel
      Left = 144
      Top = 154
      Width = 54
      Height = 13
      Caption = 'Card Width'
    end
    object pnlSortby: TPanel
      AlignWithMargins = True
      Left = 7
      Top = 46
      Width = 185
      Height = 41
      Margins.Left = 24
      Margins.Top = 12
      Margins.Right = 24
      AutoSize = True
      BevelEdges = []
      BevelOuter = bvNone
      ShowCaption = False
      TabOrder = 0
      object lblSortby: TLabel
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 185
        Height = 13
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alTop
        Caption = 'Sort by'
        ExplicitWidth = 35
      end
      object icbSort: TcxImageComboBox
        Left = 0
        Top = 13
        Margins.Left = 24
        Margins.Top = 6
        Margins.Right = 24
        Margins.Bottom = 6
        Align = alClient
        Properties.Items = <
          item
            Description = 'Description'
            ImageIndex = 0
            Value = 'p.[desc]'
          end
          item
            Description = 'Code'
            Value = 'p.code'
          end
          item
            Description = 'Price'
            Value = 'p.price'
          end>
        TabOrder = 0
        Width = 185
      end
    end
    object lbledtPicture: TLMDLabeledFileOpenEdit
      AlignWithMargins = True
      Left = 10
      Top = 242
      Width = 153
      Height = 21
      Hint = ''
      Margins.Left = 24
      Margins.Top = 24
      Margins.Right = 24
      Bevel.Mode = bmWindows
      Caret.BlinkRate = 530
      TabOrder = 1
      CustomButtons = <
        item
          Glyph.Data = {
            DE000000424DDE0000000000000076000000280000000E0000000D0000000100
            0400000000006800000000000000000000001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D00000000DDD
            DD000033333330DDDD000B033333330DDD000FB033333330DD000BFB03333333
            0D000FBFB000000000000BFBFBFBF0DDDD000FBFBFBFB0DDDD000BFB000000DD
            DD00D000DDDDDD000D00DDDDDDDDDDD00D00DDDDDDD0DD0D0D00DDDDDDDD00DD
            DD00}
          ParentFont = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Index = 0
          DisplayName = 'TLMDSpecialButton'
          ImageIndex = 0
          ListIndex = 0
          UsePngGlyph = False
        end>
      CustomButtonWidth = 18
      Filter = 'Pictuers|*.bmp;*.png;*.jpg'
      FilenameOnly = False
      EditLabel.Width = 35
      EditLabel.Height = 15
      EditLabel.Caption = 'Picture'
    end
    object seDescLines: TSpinEdit
      Left = 19
      Top = 180
      Width = 65
      Height = 22
      MaxValue = 6
      MinValue = 1
      TabOrder = 2
      Value = 6
    end
    object seCardWidth: TSpinEdit
      AlignWithMargins = True
      Left = 136
      Top = 174
      Width = 65
      Height = 22
      Margins.Top = 12
      MaxValue = 200
      MinValue = 50
      TabOrder = 3
      Value = 100
      OnChange = seCardWidthChange
    end
  end
  object btnUpdate: TButton
    Left = 811
    Top = 388
    Width = 75
    Height = 25
    Caption = 'Update'
    TabOrder = 3
    OnClick = btnUpdateClick
  end
  object pnlMisc: TPanel
    AlignWithMargins = True
    Left = 627
    Top = 231
    Width = 170
    Height = 456
    Align = alLeft
    TabOrder = 4
    ExplicitTop = 325
    ExplicitHeight = 362
    object lblMiscHeading: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 13
      Width = 162
      Height = 16
      Margins.Top = 12
      Align = alTop
      Alignment = taCenter
      Caption = 'Misc settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ExplicitWidth = 85
    end
    object lblinfo2: TLabel
      Left = 8
      Top = 60
      Width = 129
      Height = 13
      Caption = 'Use Global setting for now '
    end
    object chkshowInactive: TCheckBox
      AlignWithMargins = True
      Left = 8
      Top = 37
      Width = 97
      Height = 17
      Margins.Top = 12
      Caption = 'Show Inactive'
      TabOrder = 0
    end
    object lbledtCardPreCode: TLabeledEdit
      Left = 8
      Top = 113
      Width = 121
      Height = 21
      EditLabel.Width = 152
      EditLabel.Height = 13
      EditLabel.Caption = 'Enter Pre code {BKSP} for Case'
      TabOrder = 1
    end
    object chkUseModifier: TCheckBox
      Left = 8
      Top = 148
      Width = 97
      Height = 17
      Caption = 'Show Modifier '
      TabOrder = 2
    end
  end
  object DBNavigator1: TDBNavigator
    AlignWithMargins = True
    Left = 811
    Top = 354
    Width = 234
    Height = 25
    DataSource = dsTagGroup
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbCancel, nbRefresh]
    TabOrder = 5
  end
  object pnlBotm: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 745
    Width = 1120
    Height = 30
    Align = alBottom
    TabOrder = 6
    object lblfilter: TLabel
      AlignWithMargins = True
      Left = 833
      Top = 4
      Width = 118
      Height = 22
      Align = alRight
      Alignment = taCenter
      Caption = 'Show children of parent:'
      ExplicitHeight = 13
    end
    object btnRefresh: TSpeedButton
      AlignWithMargins = True
      Left = 1087
      Top = 4
      Width = 23
      Height = 22
      Margins.Right = 9
      Align = alRight
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000010000
        0004000000040000000500000006000000090000000B0000000B0000000B0000
        0009000000060000000300000001000000000000000000000000000000030000
        00090000000B0000000D00000010000000140000001600000017000000160000
        00140000000F0000000A0000000500000002000000000000000000000006102F
        228700000015000000180A1F1663143A29A71B5039DC1F5E43FB1B5039DC143A
        29A70A1E1662000000120000000B000000050000000100000000000000082364
        49FF133628981336289A236348FC52AB8FFF74CEB9FF88E4D3FF76D0BCFF56AD
        93FF236348FC13362897030907270000000A0000000200000001000000092669
        4EFF4BA283FF4BA283FF6ED0B2FF64B399FF4A8D76FF2C6E54FF4F8F79FF78BB
        A8FF87DBC4FF4C9F82FF15392A960000000F0000000400000001000000082A6F
        53FF91E1C9FF91E1C9FF6DB79DFF1A4534A70C1F1751020403130C1F174D1A45
        33A469A690FF97DFCAFF2A6E52FC0E241B5E0000000500000001000000072F77
        5BFFCFF3E8FFCFF3E8FF81B7A3FF183D2F8B0000000A00000005000000060103
        02111D4A39A695C5B4FF91C3B1FF1D4938A30000000600000001000000063784
        66FF378466FF378466FF378466FF378466FF1C42338200000004000000060000
        000B0F251C53378466FF378466FF2E6F55DA0000000600000002000000060000
        00110000001700000017000000120000000A00000006000000050000000A0000
        0011000000180000001C0000001A000000140000000700000002000000051B50
        39DA205F44FF205F44FF091A13570000000E0000000900000009102F2286205F
        44FF205F44FF205F44FF205F44FF205F44FF000000080000000300000004163D
        2CA25AAF97FF4DA98AFF163F2DAA0103021D0000001200000012000000151233
        2691419A7BFF60CFACFF60CFACFF236449FF0000000900000003000000020C22
        195926684DFC8EDEC9FF4D9B80FF184131AA0B1D165B020403240B1D165B1841
        31AA5DAF94FF8CE0C9FF8BE0C8FF26694EFF0000000800000003000000010000
        0005173C2D907AB19EFFAFE7D7FF77BAA3FF498F75FF2E7458FF498F75FF77BA
        A3FFAFE7D7FF7FB6A3FF7EB6A3FF2A6F53FF0000000600000002000000000000
        0002040B081C194132902E765AFC97C4B4FFB9E0D4FFCEF1E6FFB9E0D4FF97C4
        B4FF2E765AFC1A413191194132902F775BFF0000000300000001000000000000
        00000000000100000003122A215622503E9F2F6F56D9368164FA2F6F56D92250
        3E9F122A215600000005000000041B4233810000000100000001000000000000
        0000000000000000000100000001000000020000000300000004000000030000
        0002000000010000000100000001000000010000000000000000}
      OnClick = btnRefreshClick
      ExplicitLeft = 1104
    end
    object edtTestfilter: TEdit
      AlignWithMargins = True
      Left = 957
      Top = 4
      Width = 121
      Height = 22
      Margins.Right = 6
      Align = alRight
      TabOrder = 0
      ExplicitTop = 0
    end
    object btnSibling: TButton
      AlignWithMargins = True
      Left = 13
      Top = 4
      Width = 75
      Height = 22
      Margins.Left = 12
      Align = alLeft
      Caption = 'Siblings'
      TabOrder = 1
      OnClick = btnSiblingClick
    end
    object btnParent: TButton
      AlignWithMargins = True
      Left = 94
      Top = 4
      Width = 75
      Height = 22
      Align = alLeft
      Caption = 'Parent'
      TabOrder = 2
      OnClick = btnParentClick
    end
  end
  object dxDBTreeView1: TdxDBTreeView
    Left = 800
    Top = 419
    Width = 273
    Height = 155
    ShowNodeHint = False
    AutoExpand = True
    DataSource = dsTagGroup
    RootValue = Null
    SeparatedSt = ' - '
    RaiseOnError = False
    ReadOnly = True
    Indent = 19
    Color = clBtnFace
    ParentColor = False
    Options = [trCanDBNavigate, trCheckHasChildren]
    SelectedIndex = -1
    TabOrder = 7
    OnClick = dxDBTreeView1Click
  end
  object pnlDescrip: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 693
    Width = 1120
    Height = 46
    Align = alBottom
    TabOrder = 8
    object lblShowsup: TLabel
      Left = 19
      Top = 8
      Width = 36
      Height = 13
      Caption = 'Parent:'
    end
    object lblParenValue: TLabel
      Left = 61
      Top = 8
      Width = 64
      Height = 13
      Caption = 'lblParenValue'
    end
    object lbMyTag: TLabel
      Left = 16
      Top = 23
      Width = 39
      Height = 13
      Caption = 'My Tag:'
    end
    object lblMyTagValue: TLabel
      Left = 61
      Top = 23
      Width = 68
      Height = 13
      Caption = 'lblMyTagValue'
    end
  end
  object dsTagGroup: TDataSource
    DataSet = DataModule1.tblTagGroup
    OnDataChange = dsTagGroupDataChange
    Left = 984
    Top = 232
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 888
    Top = 600
  end
end

program XbrowseGroupSetup;

uses
  Vcl.Forms,
  uGroupSetupMain in 'uGroupSetupMain.pas' {Form7},
  uformGroupSetup in 'uformGroupSetup.pas' {frmGroupSetup},
  dmData in 'dmData.pas' {DataModule1: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TfrmGroupSetup, frmGroupSetup);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.Run;
end.

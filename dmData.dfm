object DataModule1: TDataModule1
  OldCreateOrder = False
  Height = 310
  Width = 570
  object tblTagGroup: TAdsTable
    DatabaseName = 'POSConnection'
    StoreActive = False
    AdsConnection = POSConnection
    TableName = 'taggroup.adt'
    Left = 474
    Top = 32
  end
  object POSConnection: TAdsConnection
    ConnectPath = 'C:\Temp\Projects\XBrowse\GroupSetup'
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    LoginPrompt = False
    StoreConnected = False
    SQLTimeout = 1
    BeforeConnect = POSConnectionBeforeConnect
    Left = 32
    Top = 16
  end
  object AdsSettings1: TAdsSettings
    DateFormat = 'MM/dd/ccyy'
    ShowDeleted = False
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    Left = 32
    Top = 120
  end
end
